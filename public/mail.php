<?php

function isLengthBetween(string $text, int $min, int $max): bool
{
    $len = strlen($text);
    return $min <= $len && $max >= $len;
}

$form = $_POST;
if ($_SERVER["CONTENT_TYPE"] === "application/json") {
    $json = file_get_contents('php://input');
    $form = json_decode($json, true);
}

if (
    !isLengthBetween($form['subject'], 10, 1000) ||
    !isLengthBetween($form['fromName'], 3, 100) ||
    !isLengthBetween($form['fromEmail'], 3, 1000) ||
    !isLengthBetween($form['message'], 20, 10000)
) {
    http_response_code(422);
    return;
}

mail(
    "karolis@kudze.lt",
    "Website: " . $form['subject'],
    "Name: " . $form['fromName'] . "\n"
    . "Email: " . $form['fromEmail'] . "\n\n"
    . $form['message']
);