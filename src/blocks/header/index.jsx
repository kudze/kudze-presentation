import "./index.scss";
import React from "react";

import Link from "../../components/link";
import Container from "../../components/container";
import Button from "../../components/button";

function scrollToTop() {
    window.scrollTo(0, 0);
}

export function Header() {
    return <div className="header">
        <Container className="header-inner">
            <Button
                href="/"
                className="logo"
                onClick={scrollToTop}
            >
                Kudze
            </Button>

            <div className="header-actions">
                <Link href="/" hoverUnderline={true}>About</Link>
                <Link href="/plugins" hoverUnderline={true}>Plugins</Link>
            </div>
        </Container>
    </div>
}