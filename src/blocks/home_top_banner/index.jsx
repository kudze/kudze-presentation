import "./index.scss";
import React from "react";

import Button, { BUTTON_SIZE_BIG, BUTTON_VARIANT_PRIMARY } from "../../components/button";
import Container from "../../components/container";
import {useInView} from "react-intersection-observer";
import classNames from "classnames";

export function HomeTopBanner() {
    const {ref, inView} = useInView({
        triggerOnce: true
    });

    const leadHeadingClasses = classNames('home-banner-lead', {'animate__animated': inView, 'animate__tada': inView})
    const subHeadingClasses = classNames('home-banner-sub', {'animate__animated': inView, 'animate__tada': inView})
    const subBelowDivClasses = classNames('home-banner-sub-below', {'animate__animated': inView, 'animate__tada': inView})

    return <div ref={ref} className="home-banner">
        <Container>
            <div className="home-banner-inner">
                <h1 className={leadHeadingClasses}>Coding with style</h1>
                <h2 className={subHeadingClasses}>and improving every day!</h2>
                <div className={subBelowDivClasses}>
                    <Button
                        variant={BUTTON_VARIANT_PRIMARY}
                        size={BUTTON_SIZE_BIG}
                        fullWidth={true}
                        href="/plugins"
                    >
                        My Shopware 6 plugins
                    </Button>
                </div>
            </div>
        </Container>
    </div>
}

export default HomeTopBanner;