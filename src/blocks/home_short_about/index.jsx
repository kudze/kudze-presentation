import React from "react";
import Container from "../../components/container";
import Link, { LINK_TARGET_BLANK } from "../../components/link";
import { Columns, Column } from "../../components/columns";

import "./index.scss";
import {useInView} from "react-intersection-observer";
import classNames from "classnames";

const LINK_FIVEMP = "http://web.archive.org/web/20170816133026/http://five-multiplayer.net/";
const LINK_NFQ = "https://www.nfq.lt/nfq-academy";
const LINK_CODE_COUNCIL = "https://codecouncil.de";
const LINK_GPSWOX = "https://www.gpswox.com/";
const LINK_VILNIUS_UNIVERSITY = "https://vu.lt/";
const LINK_SHOPWARE_6 = "https://www.shopware.com/en/products/shopware-6/";
const LINK_SHOPWARE_MARKETPLACE = "https://store.shopware.com/en/";
const LINK_SYMFONY = "https://symfony.com/";
const LINK_VUE = "https://vuejs.org/";
const LINK_MY_MANUFACTURER_PAGE = "https://store.shopware.com/en/kudzecodes.html";
const LINK_NEWSLETTER_PLUGIN = "https://store.shopware.com/en/kudze92435688677/newsletter-checkbox-plugin-for-shopware-6.html";

export function HomeShortAbout() {
    const {ref, inView} = useInView({
        triggerOnce: true
    });

    const slideInLeftClassNames = classNames({'animate__animated': inView, 'animate__fadeInLeft': inView});
    const slideInRightClassNames = classNames({'animate__animated': inView, 'animate__fadeInRight': inView});

    return <div ref={ref} className="home-short-about-bg">
        <Container className="home-short-about">
            <h1 className={classNames(slideInLeftClassNames, 'hello-heading')}>Hello!</h1>

            <Columns>
                <Column className={slideInLeftClassNames}>
                    <p>
                        I am a computer science student from Lithuania, who tries to spend his free time productively.
                        I have had an opportunity to work with several amazing people from:&nbsp;
                        <Link href={LINK_FIVEMP} target={LINK_TARGET_BLANK}>FiveMP</Link>,&nbsp;
                        <Link href={LINK_NFQ} target={LINK_TARGET_BLANK}>NFQ Academy</Link>,&nbsp;
                        <Link href={LINK_CODE_COUNCIL} target={LINK_TARGET_BLANK}>code council GmbH</Link> and&nbsp;
                        <Link href={LINK_GPSWOX} target={LINK_TARGET_BLANK}>GPSWOX</Link>.
                        I like to call myself Full-Stack developer with several years of experience.
                        Curently I am working with awesome team, from <Link href={LINK_CODE_COUNCIL} target={LINK_TARGET_BLANK}>code council GmbH</Link>.
                        Right now most of my time is being spent studying at <Link href={LINK_VILNIUS_UNIVERSITY} target={LINK_TARGET_BLANK}>Vilnius University</Link>&nbsp;
                        For that  I do not actively search new opportunities.
                        However, if you have something that would grab my interest away, do not hesitate and reach me out!
                    </p>

                    <p>
                        <Link href={LINK_SHOPWARE_6} target={LINK_TARGET_BLANK}>Shopware 6</Link> is an open headless commerce platform powered
                        by <Link href={LINK_SYMFONY} target={LINK_TARGET_BLANK}>Symfony</Link> and <Link href={LINK_VUE} target={LINK_TARGET_BLANK}>Vue.js</Link> that
                        is used by thousands of shops and supported by a huge worldwide community of developers, agencies and merchants.
                        They have big ecosystem of plugins, that expand or improve functionality of shopware platform.
                        Shop owners are able to buy the plugins from <Link href={LINK_SHOPWARE_MARKETPLACE} target={LINK_TARGET_BLANK}>Shopware store</Link> and improve shopper's experience on their shops.
                    </p>
                </Column>
                <Column className={slideInRightClassNames}>
                    <p>
                        Recently, I have started to build and sell several <Link href={LINK_SHOPWARE_6} target={LINK_TARGET_BLANK}>Shopware 6</Link> plugins.
                        I started things off simple by building a <Link href={LINK_NEWSLETTER_PLUGIN} target={LINK_TARGET_BLANK}>plugin</Link>, 
                        that adds a newsletter checkbox in the customer registration pages.&nbsp;
                        After recieving couple positive reviews and few sales, I have decided that it would be great idea to expand my plugin base.
                        Which means that you should stay tuned for awesome plugins created by me!
                        If you are interested you may check out <Link href={LINK_MY_MANUFACTURER_PAGE} target={LINK_TARGET_BLANK}>my manufacturer's page</Link> on the <Link href={LINK_SHOPWARE_MARKETPLACE}>Shopware store</Link>.
                    </p>
                </Column>
            </Columns>
        </Container>
    </div>

}

export default HomeShortAbout;