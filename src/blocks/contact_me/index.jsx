import React from "react";
import classNames from "classnames";
import { toast } from "react-toastify";
import Button, { BUTTON_SIZE_BIG, BUTTON_VARIANT_PRIMARY } from "../../components/button";
import { Column, Columns } from "../../components/columns";
import Container from "../../components/container";
import { Input, TextArea } from "../../components/input";
import "./index.scss"
import {useInView} from "react-intersection-observer";

const EMAIL_REGEX = /^[a-z0-9][a-z0-9-_.]+@([a-z]|[a-z0-9]?[a-z0-9-]+[a-z0-9])\.[a-z0-9]{2,10}(?:\.[a-z]{2,10})?$/;

export function ContactMe({heading = 'Contact me!'}) {
    const [formData, setFormData] = React.useState({
        fromEmail: "",
        fromName: "",
        subject: "",
        message: ""
    });
    const [blockAnim, setBlockAnim] = React.useState("");

    const setFromEmail = function (e) {
        setFormData({
            ...formData,
            fromEmail: e.target.value
        });
    }

    const setFromName = function (e) {
        setFormData({
            ...formData,
            fromName: e.target.value
        });
    }

    const setSubject = function (e) {
        setFormData({
            ...formData,
            subject: e.target.value
        });
    }

    const setMessage = function (e) {
        setFormData({
            ...formData,
            message: e.target.value
        })
    }

    const validate = function () {
        let errors = [];

        if (!EMAIL_REGEX.test(formData.fromEmail))
            errors.push("Email address is not valid!");

        else if (formData.fromEmail.length < 3)
            errors.push("Email address must be at least 3 characters long!");

        else if (formData.fromEmail.length > 1000)
            errors.push("Email address must be at most 1000 characters long!");

        if (formData.fromName.length < 3)
            errors.push("First name must be at least 3 characters long!");

        else if(formData.fromName.length > 100)
            errors.push("First name must be at most 100 characters long!");

        if (formData.subject.length < 10)
            errors.push("Subject name must be at least 10 characters long!");

        else if(formData.subject.length > 1000)
            errors.push("Subject name must be at most 1000 characters long!");

        if (formData.message.length < 20)
            errors.push("Message must be at least 20 characters length!");

        else if(formData.message.length > 10000)
            errors.push("Message must be at most 10000 characters length!");

        if (errors.length !== 0) {
            toast.error(errors.join("\n"), {
                position: "bottom-center",
                autoClose: 20000,
            });

            return false;
        }

        return true;
    }

    const submit = function (e) {
        if (!validate())
            return;

        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');

        fetch(
            "/mail.php",
            {
                method: 'POST',
                mode: 'same-origin',
                headers: headers,
                body: JSON.stringify(formData),
            }
        ).then(onSuccess).catch(
            () => toast.error("Unknown error happened!", {
                position: "bottom-center",
                autoClose: 5000,
            })
        );
    }

    const onSuccess = function () {
        toast.success("Your message has been sent!", {
            position: "bottom-center",
            autoClose: 5000,
        });

        setBlockAnim("animate__animated animate__bounceOutUp");
    }

    const {ref, inView} = useInView({
        threshold: 0.5
    });

    const headingClasses = classNames({'animate__animated': inView, 'animate__tada': inView});

    return <div ref={ref} className="contact-me-bg">
        <Container className={classNames("contact-me", blockAnim)}>
            <h1 className={headingClasses}>{heading}</h1>

            <Columns>
                <Column>
                    <Input id="fromEmail" type="email" value={formData.fromEmail} onChange={setFromEmail} label="Email address" />
                    <Input id="fromName" type="text" value={formData.fromName} onChange={setFromName} label="First Name" />
                    <Input id="subject" type="text" value={formData.subject} onChange={setSubject} label="Subject" />
                </Column>
                <Column>
                    <TextArea id="message" rows="5" value={formData.message} onChange={setMessage} label="Message" />
                </Column>
            </Columns>

            <Button size={BUTTON_SIZE_BIG} variant={BUTTON_VARIANT_PRIMARY} onClick={submit}>Send</Button>
        </Container>
    </div>

}

export default ContactMe;