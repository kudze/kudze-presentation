import React from "react";
import { Column, Columns } from "../../components/columns";
import Container from "../../components/container";
import Button from "../../components/button";

import YouTube from "react-youtube";

import newsletterLogo from "../../assets/plugin-newsletter.webp";
import infinityLogo from "../../assets/plugin-infinity.webp";
import sliderMediaLogo from "../../assets/plugin-slider-media.webp";

import "./index.scss";
import { useInView } from "react-intersection-observer";
import classNames from "classnames";
import { Accordion } from "../../components/accordion";

export function Plugin({
    logo,
    logoAlt = "Plugin's logotype",
    name = "default",
    headline = "",
    highlights = [],
    description = "",
    config = [],
    youtube = undefined,
    buttons = [],
    inverted = false
}) {
    const { ref, inView } = useInView({
        triggerOnce: true,
        threshold: 0.2
    });

    const slideInLeftClassNames = classNames({
        'animate__animated': inView,
        'animate__fadeInLeft': inView && !inverted,
        'animate__fadeInRight': inView && inverted,
        'd-none': !inView
    });
    const slideInRightClassNames = classNames({
        'animate__animated': inView,
        'animate__fadeInRight': inView && !inverted,
        'animate__fadeInLeft': inView && inverted,
        'd-none': !inView
    });

    const pluginClassNames = classNames('plugin', name + '-plugin', {
        'plugin-inverted': inverted
    });

    return <div ref={ref} className={`plugin-bg ${name}-plugin-bg`}>
        <Container className={pluginClassNames}>
            <Columns className={`plugin-columns ${name}-plugin-columns`}>
                <Column className={slideInLeftClassNames}>
                    <div className={`plugin-logo ${name}-plugin-logo`}>
                        <img src={logo} alt={logoAlt} />
                    </div>
                </Column>
                <Column className={slideInRightClassNames}>
                    <Accordion
                        content={
                            <>
                                {
                                    buttons.map((entry, index) => {
                                        return <Button
                                            key={index}
                                            className={`plugin-button ${name}-plugin-button`}
                                            variant="plugin"
                                            size="big"
                                            href={entry.link}
                                        >
                                            {entry.text}
                                        </Button>
                                    })
                                }

                                <Columns>
                                    <Column>
                                        <div className={`plugin-highlights ${name}-plugin-highlights`}>
                                            <p className="lead">Highlights:</p>
                                            {highlights.map((highlight, index) => <p key={index}>{highlight}</p>)}
                                        </div>
                                    </Column>

                                    {config.length !== 0 ? <Column>
                                        <div className={`plugin-configuration ${name}-plugin-configuration`}>
                                            <p className="lead">Configuration:</p>
                                            {config.map((entry, index) => <p key={index}>{entry}</p>)}
                                        </div>
                                    </Column> : null}

                                </Columns>

                                {
                                    youtube !== undefined
                                        ? <div className={`plugin-youtube ${name}-plugin-youtube`}>
                                            <YouTube opts={{ width: "100%", height: "auto" }} videoId={youtube} />
                                        </div> : null
                                }
                            </>
                        }
                        heading={
                            <>
                                <h1 className={`plugin-headline ${name}-plugin-headline`}>
                                    {headline}
                                </h1>

                                <div className={`plugin-description ${name}-plugin-description`}>
                                    <p>{description}</p>
                                </div>
                            </>
                        }
                    />
                </Column>
            </Columns>
        </Container>
    </div>
}

export function NewsletterPlugin() {
    return <Plugin
        logo={newsletterLogo}
        logoAlt="Newsletter plugin's logotype!"
        headline="Newsletter checkbox plugin for Shopware 6"
        highlights={[
            "Increase your newsletter registration rates!",
            "Gain more newsletter participants!",
            "Get configurable newsletter checkbox!"
        ]}
        description={`
            This plugin adds a newsletter checkbox at Shopware 6 account registration, and checkout registration pages.
            The plugin works both with supports guests users and with registered users.
            If your shop is using email verification for newsletter participants, the user is only added to newsletter after he has verified his email after registration.
            If you're not using email verification user is added right after registeration process.
        `}
        config={[
            "You may have newsletter checkbox checked automatically.",
            "You may remove newsletter checkbox from checkout reigster page.",
            "You may make newsletter checkbox box inacessible for guest users."
        ]}
        buttons={[
            {
                text: "Check it out on Shopware store",
                link: "https://store.shopware.com/en/kudze92435688677/newsletter-checkbox-plugin-for-shopware-6.html"
            },
            {
                text: "Check demo out",
                link: "https://sw6-newsletter-demo.kudze.lt/account/login"
            },
            {
                text: "Documentation",
                link: "https://gitlab.com/kudzecodes/documentation/sw6-kudze-newsletter-checkbox-plugin-docs/-/blob/main/README.md"
            }
        ]}
        youtube="pbBwFw8gUqg"
        name="newsletter"
    />;
}

export function InfinityScrollPlugin() {
    return <Plugin
        logo={infinityLogo}
        logoAlt="Infinity scroll plugin logotype"
        headline="Infinity Scroll Plugin for Shopware 6"
        highlights={[
            "Increase your shop's conversion rate by providing your customers with seamless paging experience!",
            "Plugin will also work without any issues if n-th page is loaded first!"
        ]}
        config={[
            "Each product listing element can be configured separately:",
            "You can enable/disable infinity scroll effect.",
            "You can hide/show pagination.",
            "You can enable/disable feature that adds created pages to pagination.",
            "You can enable/disable feature that marks loaded pages as active in pagination.",
            "You can enable/disable sticky products listing actions. (Ordering and pagination will appear in sticky section).",
            "You can configure maximum number of pages that the plugin will load.",
            "You can enable/disable feature that scrolls to first page after any filter updates.",
            "You can enable/disable feature that scrolls to first page after ordering change.",
            "You can enable/disable feature that scrolls to active page in pagination.",
            "You can change default timeout before user is scrolled to n-th page.",
            "You can change default timeout before intersection observer is initialized."
        ]}
        description="This plugin adds infinity scroll effect to product listing element."
        name="infinity"
        buttons={[
            {
                text: "Check it out on Shopware store",
                link: "https://store.shopware.com/en/kudze39419365022/infinity-scroll-plugin-for-shopware-6.html"
            },
            {
                text: "Check demo out",
                link: "https://sw6-infinity-scroll-demo.kudze.lt/"
            }
        ]}
        inverted={true}
    />
}

export function KudzeImageSliderUtility() {
    return <Plugin
        logo={sliderMediaLogo}
        logoAlt="Kudze Image Slider & Gallery Sorting Utility"
        headline="Kudze Image Slider & Gallery Sorting Utility"
        highlights={[
            "Save your time!",
        ]}
        description={'Adds ability to perform "move up" , "move down" and "swap" (via drag and drop) actions on media files already uploaded both in "image-slider" and "image-gallery" cms element configuration in administration panel!'}
        name="infinity"
        buttons={[
            {
                text: "Check it out on Shopware store",
                link: "https://store.shopware.com/en/kudze45275047093/kudze-image-slider-gallery-sorting-utility.html"
            }
        ]}
        inverted={false}
    />
}

export function Plugins() {
    return <>
        <NewsletterPlugin />
        <InfinityScrollPlugin />
        <KudzeImageSliderUtility />
    </>
}

export default Plugins;