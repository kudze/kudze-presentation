import {useInView} from 'react-intersection-observer';

import {Link} from "../../components/link";
import {Column, Columns} from "../../components/columns";

import "./index.scss";
import classNames from "classnames";


const LINK_GITHUB = "https://github.com/Kudze";
const LINK_GITLAB = "https://gitlab.com/kudze";
const LINK_LINKEDIN = "https://www.linkedin.com/in/kudze/";
const LINK_EMAIL = "mailto: karolis@kudze.lt";

export function Footer() {
    const {ref, inView} = useInView({
        triggerOnce: true
    });

    const navigationClasses = classNames('navigation', {'animate__animated': inView, 'animate__fadeInLeft': inView});
    const contactsClasses = classNames('navigation', {'animate__animated': inView, 'animate__fadeInRight': inView});
    const copyrightClasses = classNames('copyright', {'animate__animated': inView, 'animate__fadeInUp': inView});

    return <footer ref={ref} className="footer">
        <Columns className="footer-columns" collapse={false}>
            <Column className={navigationClasses}>
                <p className="nav-title">Navigation</p>
                <Link href="/">About</Link>
                <Link href="/plugins">Plugins</Link>
            </Column>

            <Column className={contactsClasses}>
                <p className="nav-title">Contacts</p>
                <Link href={LINK_GITHUB} target="_blank">GitHub</Link>
                <Link href={LINK_GITLAB} target="_blank">GitLab</Link>
                <Link href={LINK_LINKEDIN} target="_blank">LinkedIn</Link>
                <Link href={LINK_EMAIL} target="_blank">Email</Link>
            </Column>
        </Columns>

        <div className={copyrightClasses}>
            &copy; 2022 - {new Date().getFullYear()}. All rights reserved.
        </div>
    </footer>
}