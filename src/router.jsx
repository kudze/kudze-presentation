import React from "react";
import About from "./pages/About";
import Plugins from './pages/Plugins';
import { BrowserRouter, Route, Routes } from "react-router-dom";

export function Router() {
    return <BrowserRouter>
        <Routes>
            <Route path="/" element={<About />} />
            <Route path="/plugins/" element={<Plugins />} />
        </Routes>
    </BrowserRouter>;
}

export default Router;