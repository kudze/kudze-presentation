import React from 'react';
import {createRoot} from 'react-dom/client';
import {ToastContainer} from 'react-toastify';
import Router from "./router";

import "animate.css";
import {Cookies} from "./components/cookies/index.jsx";


const container = document.getElementById('root');
const root = createRoot(container);

root.render(
    <React.StrictMode>
        <Router/>
        <ToastContainer
            position="bottom-center"
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            theme='dark'
        />
        <Cookies/>
    </React.StrictMode>
);