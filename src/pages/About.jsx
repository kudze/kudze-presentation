import React from "react";
import { Header } from "../blocks/header";
import HomeTopBanner from "../blocks/home_top_banner";
import HomeShortAbout from "../blocks/home_short_about";
import { Footer } from "../blocks/footer";
import ContactMe from "../blocks/contact_me";

export function About() {
    return (
        <>
            <Header />
            <HomeTopBanner />
            <HomeShortAbout />
            <ContactMe />
            <Footer />
        </>
    );
}

export default About;