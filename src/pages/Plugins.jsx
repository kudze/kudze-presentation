import React from "react";
import { Header } from "../blocks/header";
import { Footer } from "../blocks/footer";
import ContactMe from "../blocks/contact_me";
import PluginsBlock from "../blocks/plugins";

export function Plugins() {
    return (
        <>
            <Header />
            <PluginsBlock/>
            <ContactMe heading="In a need of custom extension? Contact us!"/>
            <Footer />
        </>
    );
}

export default Plugins;