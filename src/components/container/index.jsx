import classNames from "classnames";
import React from "react";

import "./index.scss";

export function Container({
    children = "",
    className = ""
}) {
    className = classNames('container', className);

    return <div className={className}>{children}</div>
}

export default Container;