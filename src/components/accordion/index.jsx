import { useState } from "react"

import { Button } from "../button";

export function Accordion({ heading, content, defaultExpanded = false }) {
    const [expanded, setExpanded] = useState(defaultExpanded);

    if (expanded)
        return <>
            {heading}
            <div className="animate__animated animate__fadeIn">
                {content}
            </div>
        </>

    return <>
        {heading}
        <Button variant="plugin" size="big" className={`plugin-button`} onClick={setExpanded.bind(this, true)}>
            Click to read more about this extension
        </Button>
    </>

}