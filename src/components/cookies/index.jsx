import React, {useEffect} from "react";
import {CookieConsent, Cookies as CookiesUtil, getCookieConsentValue} from "react-cookie-consent";
import {initGA} from "../../api/ga.js";
import "./index.scss";

export function Cookies() {
    function onAccept() {
        initGA();
    }

    function onDecline() {
        CookiesUtil.remove("_ga");
        CookiesUtil.remove("_gat");
        CookiesUtil.remove("_gid");
    }

    useEffect(() => {
        const isConsent = getCookieConsentValue();
        if (isConsent === "true") {
            onAccept();
        }
    }, []);

    return <CookieConsent
        enableDeclineButton
        disableStyles
        containerClasses={"container cookie-container"}
        buttonWrapperClasses={"cookie-container-buttons"}
        contentClasses={"cookie-container-content"}
        onAccept={onAccept}
        onDecline={onDecline}
        buttonText={"Accept"}
        declineButtonText={"Decline"}
    >
        This website uses cookies to enhance the user experience.
    </CookieConsent>
}