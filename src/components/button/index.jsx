import classNames from "classnames";
import React from "react";
import { useNavigate } from "react-router-dom";
import "./index.scss";

export const BUTTON_SIZE_NORMAL = "normal";
export const BUTTON_SIZE_BIG = "big";

export const BUTTON_VARIANT_PRIMARY = "primary";

export function Button({
    children = "",
    size = "",
    variant = "",
    className = "",
    href = "",
    fullWidth = false,
    onClick = function () { },
}) {
    const navigate = useNavigate();

    className = classNames(variant, size, className);

    if (fullWidth)
        className = classNames("full-width", className);

    let onClickMiddleware = onClick;
    if (href !== null)
        onClickMiddleware = function () {
            if (href.startsWith("https://" || href.startsWith("http://")))
                window.location = href;

            else
                navigate(href);

            onClick();
        }


    return <button
        className={className}
        onClick={onClickMiddleware}
    >
        {children}
    </button>;
}

export default Button;