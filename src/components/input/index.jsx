import React from "react";
import "./index.scss";

export function Input({
    id = "",
    value = "",
    type = "text",
    label = "",
    onChange = function () { },
}) {
    if (label === "")
        label = null;
    else
        label = <label htmlFor={id}>{label}</label>;

    return <div className="input-group">
        <input id={id} onChange={onChange} value={value} type={type} placeholder=" " />
        {label}
    </div>
}

export function TextArea({
    id = "",
    value = "",
    type = "text",
    label = "",
    onChange = function () { },
}) {
    if (label === "")
        label = null;
    else
        label = <label htmlFor={id}>{label}</label>;

    return <div className="input-group">
        <textarea id={id} onChange={onChange} value={value} type={type} placeholder=" " />
        {label}
    </div>
}