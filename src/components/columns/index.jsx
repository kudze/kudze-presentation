import React from "react"
import classNames from "classnames";
import "./index.scss"

export function Columns({
    className = "",
    collapse = true,
    children,
}) {
    className = classNames("columns", className);

    if (collapse)
        className = classNames("collapse", className);

    return <div className={className}>
        {children}
    </div>
}

export function Column({
    className = "",
    children,
}) {
    className = classNames("column", className);

    return <div className={className}>
        {children}
    </div>;
}