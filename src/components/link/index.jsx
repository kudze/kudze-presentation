import "./index.scss"
import React from "react";
import classNames from "classnames";
import { useLocation } from "react-router-dom";

export const LINK_TARGET_BLANK = "_blank";
export const LINK_TARGET_NORMAL = "_self";

export function Link(
    {
        href = "#",
        className = "",
        children = "Empty",
        hoverUnderline = false,
        target=LINK_TARGET_NORMAL,
        rel=""
    }
) {
    const location = useLocation();
    if(location.pathname === href)
        className = classNames("active", className);

    if(rel === "" && target === LINK_TARGET_BLANK)
        rel = "noopener";

    if (hoverUnderline)
        className = classNames("hover-underline", className);

    return <a
        href={href}
        className={className}
        target={target}
        rel={rel}
    >
        {children}
    </a>;
}

export default Link;